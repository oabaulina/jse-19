package ru.baulina.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Project extends AbstractEntity implements Serializable {

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Long userId;

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
