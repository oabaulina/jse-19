package ru.baulina.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigInteger;
import java.security.MessageDigest;

@UtilityClass
public class HashUtil {

    private static final int ITERATION = 30333;

    private static final String SECRET = "753951852456";

    @Nullable
    public static String salt(final String value) {
        if (value == null) return null;
        @NotNull String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5hashing(SECRET + value + SECRET);
        }
        return result;
    }

    @NotNull
    static String md5hashing(final String text) {
        @Nullable String hashtext = null;
        try
        {
            @NotNull final String plaintext = text;
            @NotNull MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(plaintext.getBytes());
            final byte[] digest = md.digest();
            @NotNull BigInteger bigInt = new BigInteger(1, digest);
            hashtext = bigInt.toString(16);
            while(hashtext.length() < 32 ){
                hashtext = "0" + hashtext;
            }
        } catch (Exception e)
        {
            System.out.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return hashtext;
    }

}
