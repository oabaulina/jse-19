package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    @NotNull
    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(@NotNull final Long userId, @NotNull final Task task) {
        task.setUserId(userId);
        merge(task);
    }

    @Override
    public void remove(@NotNull final Long userId, @NotNull final Task task) {
        if (userId.equals(task.getUserId())) tasks.remove(task);
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final Long userId) {
        @NotNull final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @NotNull
    @Override
    public List<Task> getListTask() {
        @NotNull final List<Task> result = new ArrayList<>(tasks);
        return result;
    }

    @Override
    public void clear(@NotNull final Long userId) {
        tasks.removeIf(task -> userId.equals(task.getUserId()));
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final Long userId, @NotNull final Long id) {
        for (@NotNull final Task task: tasks) {
            if (userId.equals(task.getUserId()) && id.equals(task.getId())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final Long userId, @NotNull final Integer index) {
        @NotNull final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result.get(index);
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final Long userId, @NotNull final String name) {
        for (@NotNull final Task task: tasks) {
            if (userId.equals(task.getUserId()) && name.equals(task.getName())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final Long userId, @NotNull final Long id) {
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@NotNull final Long userId, @NotNull final Integer index) {
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByName(@NotNull final Long userId, @NotNull final String name) {
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public void merge(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        for (final Task task: tasks) merge(task);
    }

    @Override
    public void merge(@Nullable final Task... tasks) {
        if (tasks == null) return;
        for (final Task task: tasks) merge(task);
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        tasks.add(task);
    }

    @Override
    public void load(@NotNull final List<Task> tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public void load(@NotNull final Task... tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

}
