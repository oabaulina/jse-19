package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.api.repository.IUserRepository;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    @NotNull
    private final List<User> users = new ArrayList<>();

    @NotNull
    @Override
    public List<User> findAll() {
        return users;
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) {
        users.add(user);
        return user;
    }

    @Nullable
    @Override
    public User findById(@NotNull final Long id) {
        for (final User user: users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User removeUser(@NotNull final User user) {
        users.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        for (final User user: users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User removeById(@NotNull final Long id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        if (user == null) return;
        removeUser(user);
    }

    @Override
    public void merge(@NotNull final List<User> users) {
        for (final User user: users) merge(user);
    }

    @Override
    public void merge(@NotNull final User... users) {
        for (final User user: users) merge(user);
    }

    @Nullable
    @Override
    public User merge(@Nullable final User user) {
        if (user == null) return null;
        users.add(user);
        return user;
    }

    @Override
    public void load(@NotNull final List<User> users) {
        clear();
        merge(users);
    }

    @Override
    public void load(@NotNull final User... users) {
        clear();
        merge(users);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @NotNull
    @Override
    public List<User> getListUser() {
        @NotNull final List<User> result = new ArrayList<>(users);
        return result;
    }

}
