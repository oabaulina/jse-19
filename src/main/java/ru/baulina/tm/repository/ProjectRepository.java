package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(@NotNull final Long userId, @NotNull final Project project) {
        project.setUserId(userId);
        merge(project);
    }

    @Override
    public void remove(@NotNull final Long userId, @NotNull final Project project) {
        if (userId.equals(project.getUserId())) projects.remove(project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final Long userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @NotNull
    @Override
    public List<Project> getListProject() {
        @NotNull final List<Project> result = new ArrayList<>(projects);
        return result;
    }

    @Override
    public void clear(@NotNull final Long userId) {
        projects.removeIf(project -> userId.equals(project.getUserId()));
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final Long userId, @NotNull final Long id) {
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId()) && id.equals(project.getId())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final Long userId, @NotNull final Integer index) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result.get(index);
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final Long userId, @NotNull final String name) {
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId()) && name.equals(project.getName())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project removeOneById(@NotNull final Long userId, @NotNull final Long id) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@NotNull final Long userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByName(@NotNull final Long userId, @NotNull final String name) {
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Override
    public void merge(@Nullable final List<Project> projects) {
        if (projects == null) return;
        for (final Project project: projects) merge(project);
    }

    @Override
    public void merge(@Nullable final Project... projects) {
        if (projects == null) return;
        for (final Project project: projects) merge(project);
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        projects.add(project);
    }

    @Override
    public void load(@NotNull final List<Project> projects) {
        clear();
        merge(projects);
    }

    @Override
    public void load(@NotNull final Project... projects) {
        clear();
        merge(projects);
    }

    @Override
    public void clear() {
        projects.clear();
    }

}
