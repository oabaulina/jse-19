package ru.baulina.tm;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.bootstrap.Bootstrap;

public class App {

    public static void main(@NotNull final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
