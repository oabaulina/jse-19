package ru.baulina.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.api.repository.IUserRepository;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.util.HashUtil;

import java.util.List;

@RequiredArgsConstructor
public class UserService implements IUserService{

    private final IUserRepository userRepository;

    @Override
    public @NotNull List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(@Nullable final Long id) {
        if (id == null || id < 0) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public User removeById(@Nullable final Long id) {
        if (id == null || id < 0) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @Nullable
    @Override
    public User removeUser(@Nullable final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public void load(final List<User> users) {
        userRepository.load(users);
    }

    @Override
    public void lockUserLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
    }

    @Override
    public void unlockUserLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
    }

    @NotNull
    @Override
    public List<User> getListUser() {
        return userRepository.getListUser();
    }

}
