package ru.baulina.tm.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.api.service.IAuthService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.user.AccessDeniedException;
import ru.baulina.tm.exception.empty.EmptyLoginException;
import ru.baulina.tm.exception.empty.EmptyPasswordException;
import ru.baulina.tm.util.HashUtil;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public class AuthService implements IAuthService {

    private final IUserService userService;

    private Long userId;

    @NotNull
    @Override
    public Long getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void checkRole(@Nullable final Role[] roles) {
        if (roles == null || roles.length == 0) return;
        final Long userId = getUserId();
        final User user = userService.findById(userId);
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item: roles) {
            if (role.equals(item)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void isAuth() {
        if (userId == null) throw new AccessDeniedException();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        userService.create(login, password, email);
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User currentUser = userService.findById(userId);
        final User userShow = userService.findByLogin(login);
        if (userShow.getId() == userId || currentUser.getLogin().equals("admin")) { return userShow; }
        else { throw new AccessDeniedException(); }
    }

    @Nullable
    @Override
    public User findById() {
        return userService.findById(userId);
    }

    @Override
    public void changePassword(@Nullable final String passwordOld, @Nullable final String passwordNew) {
        final User user = findById();
        if (user == null) throw new AccessDeniedException();
        final String hashOld = HashUtil.salt(passwordOld);
        if (hashOld == null) throw new AccessDeniedException();
        if (!hashOld.equals(user.getPasswordHash())) throw new AccessDeniedException();
        final String hashNew = HashUtil.salt(passwordNew);
        user.setPasswordHash(hashNew);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        final User user = userService.findById(userId);
        if (!user.getLogin().equals("admin")) throw new AccessDeniedException();
        return userService.findAll();
    }

    @Override
    public void changeUser(@Nullable final String email, @Nullable final String festName, @Nullable final String LastName) {
        final User user = findById();
        user.setEmail(email);
        user.setFirstName(festName);
        user.setLastName(LastName);
    }
}
