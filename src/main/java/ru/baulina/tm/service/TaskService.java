package ru.baulina.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.exception.empty.EmptyIdException;
import ru.baulina.tm.exception.empty.EmptyPasswordException;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;
import ru.baulina.tm.exception.empty.EmptyNameException;
import ru.baulina.tm.entity.Task;

import java.util.List;

@RequiredArgsConstructor
public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    @Override
    public void create(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId,task);
    }

    @Override
    public void create(
            @Nullable final Long userId, @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public void add(@Nullable final Long userId, @Nullable final Task task) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (task == null) return;
        taskRepository.add(userId, task);
    }

    @Override
    public void remove(@Nullable final Long userId, @Nullable final Task task) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (task == null) return;
        taskRepository.remove(userId, task);
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
         return taskRepository.findAll(userId);
    }

    @NotNull
    @Override
    public List<Task> getListTask() {
        return taskRepository.getListTask();
    }

    @Override
    public void clear(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        taskRepository.clear(userId);
    }

    @Override
    public void load(@Nullable List<Task> tasks) {
        taskRepository.clear();
        if (tasks == null) {return;}
        taskRepository.load(tasks);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final Long userId, @Nullable final Long id) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (id == null || id < 0) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public Task findOneByIndex(@Nullable final Long userId, @Nullable final Integer index) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public Task findOneByName(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    public Task removeOneById(@Nullable final Long userId, @Nullable final Long id) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
         if (id == null || id < 0) throw new EmptyIdException();
         return taskRepository.removeOneById(userId, id);
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@Nullable final Long userId, @Nullable final Integer index) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public Task removeOneByName(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(userId, name);
    }

    @Nullable
    @Override
    public Task updateTaskById(
            @Nullable final Long userId, @Nullable final Long id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Nullable
    @Override
    public Task updateTaskByIndex(
            @Nullable final Long userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyPasswordException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return null;
    }

}
