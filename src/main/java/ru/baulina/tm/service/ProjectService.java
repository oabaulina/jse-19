package ru.baulina.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.exception.empty.EmptyIdException;
import ru.baulina.tm.exception.empty.EmptyPasswordException;
import ru.baulina.tm.exception.empty.EmptyUserIdException;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;
import ru.baulina.tm.exception.empty.EmptyNameException;
import ru.baulina.tm.entity.Project;

import java.util.List;

@RequiredArgsConstructor
public class ProjectService implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    @Override
    public void create(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);

    }

    @Override
    public void create(
            @Nullable final Long userId, @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void add(@Nullable final Long userId, @Nullable final Project project) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(@Nullable final Long userId, @Nullable final Project project) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        return projectRepository.findAll(userId);
    }

    @NotNull
    @Override
    public List<Project> getListProject() {
        return projectRepository.getListProject();
    }

    @Override
    public void clear(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

    @Override
    public void load(@Nullable List<Project> projects) {
        projectRepository.clear();
        if (projects == null) {return;}
        projectRepository.load(projects);
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final Long userId, @Nullable final Long id) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        return projectRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public Project findOneByIndex(@Nullable final Long userId, @Nullable final Integer index) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project findOneByName(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    public Project removeOneById(@Nullable final Long userId, @Nullable final Long id) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        return projectRepository.removeOneById(userId, id);
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@Nullable final Long userId, @Nullable final Integer index) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project removeOneByName(@Nullable final Long userId, @Nullable final String name) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Nullable
    @Override
    public Project updateTaskById(
            @Nullable final Long userId, @Nullable final Long id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Nullable
    @Override
    public Project updateTaskByIndex(
            @Nullable final Long userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = findOneByIndex(userId, index);

       if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return null;
    }

}
