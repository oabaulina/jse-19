package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.entity.Project;

import java.util.List;

public final class ProjectShowCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task project.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        @NotNull final Long userId = serviceLocator.getAuthService().getUserId();
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
