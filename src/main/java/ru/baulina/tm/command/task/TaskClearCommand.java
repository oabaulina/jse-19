package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASK]");
        @NotNull final Long userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
        System.out.println();
    }

}
