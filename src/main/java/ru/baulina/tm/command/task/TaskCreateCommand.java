package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        @Nullable final  String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final  String description = TerminalUtil.nextLine();
        @Nullable final Long userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
