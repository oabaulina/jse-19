package ru.baulina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.api.service.IDomainService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.exception.data.DataSaveException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public final class DataXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA XML SAVE]");
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        @NotNull final Domain domain = new Domain();
        domainService.export(domain);
        @NotNull final File file = new File(DataConstant.FILE_XML);
        try (
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        ) {
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
        } catch (IOException e) {
            throw new DataSaveException(e);
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
