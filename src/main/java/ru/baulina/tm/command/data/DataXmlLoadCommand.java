package ru.baulina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.exception.data.DataLoadException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public final class DataXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from xml file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA XML LOAD]");
        @NotNull final File file = new File(DataConstant.FILE_XML);
        try (
            @NotNull final FileInputStream fileInputStream = new FileInputStream(file)
        ) {
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
            @NotNull final IProjectService projectService = serviceLocator.getProjectService();
            @NotNull final ITaskService taskService = serviceLocator.getTaskService();
            @NotNull final IUserService userService = serviceLocator.getUserService();
            taskService.load(domain.getTasks());
            userService.load(domain.getUsers());
            projectService.load(domain.getProjects());
        } catch (IOException e) {
            throw new DataLoadException(e);
        }
        System.out.println("[LOGOUT CURRENT USER]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
        System.out.println();
    }

}
