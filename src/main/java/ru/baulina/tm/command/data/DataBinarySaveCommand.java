package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.api.service.IDomainService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.exception.data.DataDeleteException;
import ru.baulina.tm.exception.data.DataSaveException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-bin-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BINARY SAVE]");
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        @NotNull final Domain domain = new Domain();
        domainService.export(domain);
        @NotNull final File file = new File(DataConstant.FILE_BINARY);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
        } catch (IOException e) {
            throw new DataDeleteException();
        }
        try (
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        ){
            objectOutputStream.writeObject(domain);
        } catch (IOException e) {
            e.fillInStackTrace();
            throw new DataSaveException(e);
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
