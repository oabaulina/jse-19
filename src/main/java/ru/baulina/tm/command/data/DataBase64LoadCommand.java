package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.exception.data.DataLoadException;

import java.io.*;
import java.nio.file.Files;
import java.util.Base64;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-base64-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from base64 file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        try {
            @NotNull final byte[] bytesEncoded = Files.readAllBytes(file.toPath());
            @NotNull final byte[] bytes = Base64.getDecoder().decode(bytesEncoded);
            @NotNull final ByteArrayInputStream byteInputStream = new ByteArrayInputStream(bytes);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            @NotNull final IProjectService projectService = serviceLocator.getProjectService();
            @NotNull final ITaskService taskService = serviceLocator.getTaskService();
            @NotNull final IUserService userService = serviceLocator.getUserService();
            projectService.load(domain.getProjects());
            taskService.load(domain.getTasks());
            userService.load(domain.getUsers());
            objectInputStream.close();
            byteInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            throw new DataLoadException(e);
        }
        System.out.println("[LOGOUT CURRENT USER]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
        System.out.println();
    }

}
