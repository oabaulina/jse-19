package ru.baulina.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.exception.data.DataLoadException;

import java.io.*;
import java.util.Arrays;

public final class DataJsonLoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load json from binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA JSON LOAD]");
        final File file = new File(DataConstant.FILE_JSON);
        try (
            @NotNull final FileInputStream fileInputStream = new FileInputStream(file)
        ) {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
            @NotNull final IProjectService projectService = serviceLocator.getProjectService();
            @NotNull final ITaskService taskService = serviceLocator.getTaskService();
            @NotNull final IUserService userService = serviceLocator.getUserService();
            projectService.load(domain.getProjects());
            taskService.load(domain.getTasks());
            userService.load(domain.getUsers());
        } catch (IOException e) {
            throw new DataLoadException(e);
        }
        System.out.println("[LOGOUT CURRENT USER]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
        System.out.println();
    }

}
