package ru.baulina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.command.AbstractCommand;

public final class LogoutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Log out.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
        System.out.println();
    }

}
