package ru.baulina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.util.TerminalUtil;

public final class ProfileOfUserCommand extends AbstractAuthCommand {

    @NotNull
    @Override
    public String name() {
        return "profiler-of-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Show user's profiler.";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().isAuth();
        System.out.println("[SHOW_PROFILE_OF_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findByLogin(login);
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD: " + user.getPasswordHash());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FEST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("[OK]");
        System.out.println();
    }

}
