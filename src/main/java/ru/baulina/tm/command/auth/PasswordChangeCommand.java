package ru.baulina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.util.TerminalUtil;

public final class PasswordChangeCommand extends AbstractAuthCommand {

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Change user's password.";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().isAuth();
        System.out.println("[CHANGE_PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        final String passwordOld = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        final String passwordNew = TerminalUtil.nextLine();
        serviceLocator.getAuthService().changePassword(passwordOld, passwordNew);
        System.out.println("[OK]");
        System.out.println();
    }

}
