package ru.baulina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.entity.User;

import java.util.List;

public final class UserShowCommand extends AbstractAuthCommand {

    @NotNull
    @Override
    public String name() {
        return "list-users";
    }

    @NotNull
    @Override
    public String description() {
        return "Show users.";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().isAuth();
        final List<User> users = serviceLocator.getUserService().findAll();
        System.out.println("[SHOW_LIST_USERS]");
        int index = 1;
        for (User user: users) {
            System.out.println(index + ". ");
            System.out.println("LOGIN: " + user.getLogin());
            System.out.println("PASSWORD: " + user.getPasswordHash());
            System.out.println("E-MAIL: " + user.getEmail());
            System.out.println("FEST NAME: " + user.getFirstName());
            System.out.println("LAST NAME: " + user.getLastName());
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
