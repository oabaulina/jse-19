package ru.baulina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add(@NotNull final Long userId, @NotNull final Task task);

    void remove(@NotNull final Long userId, @NotNull final Task task);

    @NotNull
    List<Task> findAll(@NotNull final Long userId);

    @NotNull
    List<Task> getListTask();

    void clear(@NotNull final Long userId);

    @Nullable
    Task findOneById(@NotNull final Long userId, @NotNull final Long id);

    @Nullable
    Task findOneByIndex(@NotNull final Long userId, @NotNull final Integer index);

    @Nullable
    Task findOneByName(@NotNull final Long userId, @NotNull final String name);

    @Nullable
    Task removeOneById(@NotNull final Long userId, @NotNull final Long id);

    @Nullable
    Task removeOneByIndex(@NotNull final Long userId, @NotNull final Integer index);

    @Nullable
    Task removeOneByName(@NotNull final Long userId, @NotNull final String name);

    void merge(@Nullable final List<Task> tasks);

    void merge(@Nullable final Task... tasks);

    void merge(@Nullable final Task task);

    void load(@NotNull final List<Task> tasks);

    void load(@NotNull final Task... tasks);

    void clear();

}
