package ru.baulina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @NotNull
    List<User> findAll();

    @NotNull
    User add(@NotNull final User user);

    @Nullable
    User findById(@NotNull final Long id);

    @Nullable
    User findByLogin(@NotNull final String login);

    @Nullable
    User removeUser(@NotNull final User user);

    @Nullable
    User removeById(@NotNull final Long id);

    void removeByLogin(@NotNull final String login);

    void merge(@NotNull final List<User> users);

    void merge(@NotNull final User... users);

    @Nullable
    User merge(@Nullable final User task);

    void load(@NotNull final List<User> users);

    void load(@NotNull final User... users);

    void clear();

    @NotNull
    List<User> getListUser();

}
