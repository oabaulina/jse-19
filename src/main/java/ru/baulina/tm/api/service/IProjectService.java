package ru.baulina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void create(@Nullable final Long userId, @Nullable final String name);

    void create(
            @Nullable final Long userId, @Nullable final String name,
            @Nullable final String description
    );

    void add(@Nullable final Long userId, @Nullable final Project project);

    void remove(@Nullable final Long userId, @Nullable final Project project);

    @NotNull
    List<Project> findAll(@Nullable final Long userId);

    @NotNull
    List<Project> getListProject();

    void clear(@Nullable final Long userId);

    void load(@Nullable final List<Project> projects);

    @Nullable
    Project findOneById(@Nullable final Long userId, @Nullable final Long id);

    @Nullable
    Project findOneByIndex(@Nullable final Long userId, @Nullable final Integer index);

    @Nullable
    Project findOneByName(@Nullable final Long userId, @Nullable final String name);

    @Nullable
    Project removeOneById(@Nullable final Long userId, @Nullable final Long id);

    @Nullable
    Project removeOneByIndex(@Nullable final Long userId, @Nullable final Integer index);

    @Nullable
    Project removeOneByName(@Nullable final Long userId, @Nullable final String name);

    @Nullable
    Project updateTaskById(
            @Nullable final Long userId, @Nullable final Long id,
            @Nullable final String name, @Nullable final String  description
    );

    @Nullable
    Project updateTaskByIndex(
            @Nullable final Long userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String  description
    );

}
