package ru.baulina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import java.util.List;

public interface IUserService {

    @NotNull
    List<User> findAll();

    @NotNull
    User create(@Nullable final String login, @Nullable final String password);

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final String email);

    @NotNull
    User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role);

    void load(@Nullable final List<User> users);

    @Nullable
    User findById(@Nullable final Long id);

    @Nullable
    User findByLogin(@Nullable final String login);

    @Nullable
    User removeUser(@Nullable final User user);

    @Nullable
    User removeById(@Nullable final Long id);

    void removeByLogin(@Nullable final String login);

    void lockUserLogin(@Nullable final String login);

    void unlockUserLogin(@Nullable final String login);

    @NotNull
    List<User> getListUser();

}
